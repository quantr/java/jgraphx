package hk.quantr.mxgraph.shape;

import hk.quantr.mxgraph.canvas.mxGraphics2DCanvas;
import hk.quantr.mxgraph.view.mxCellState;

public interface mxIShape
{
	/**
	 * 
	 */
	void paintShape(mxGraphics2DCanvas canvas, mxCellState state);

}
