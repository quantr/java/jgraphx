package hk.quantr.mxgraph.shape;

import hk.quantr.mxgraph.canvas.mxGraphics2DCanvas;
import hk.quantr.mxgraph.util.mxPoint;
import hk.quantr.mxgraph.view.mxCellState;

public interface mxIMarker
{
	/**
	 * 
	 */
	mxPoint paintMarker(mxGraphics2DCanvas canvas, mxCellState state, String type,
			mxPoint pe, double nx, double ny, double size, boolean source);

}
