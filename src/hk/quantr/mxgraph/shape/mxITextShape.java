/**
 * Copyright (c) 2010, Gaudenz Alder, David Benson
 */
package hk.quantr.mxgraph.shape;

import java.util.Map;

import hk.quantr.mxgraph.canvas.mxGraphics2DCanvas;
import hk.quantr.mxgraph.view.mxCellState;

public interface mxITextShape
{
	/**
	 * 
	 */
	void paintShape(mxGraphics2DCanvas canvas, String text, mxCellState state,
			Map<String, Object> style);

}
