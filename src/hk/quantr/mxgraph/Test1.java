package hk.quantr.mxgraph;

import hk.quantr.mxgraph.model.mxCell;
import hk.quantr.mxgraph.model.mxGeometry;
import hk.quantr.mxgraph.swing.mxGraphComponent;
import hk.quantr.mxgraph.util.mxConstants;
import hk.quantr.mxgraph.view.mxEdgeStyle;
import hk.quantr.mxgraph.view.mxGraph;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Map;
import javax.swing.JFrame;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Test1 {

	static class MyGraphComponent extends mxGraphComponent {

		public MyGraphComponent(mxGraph graph) {
			super(graph);
		}

		@Override
		protected void paintGrid(Graphics g) {
			double scale = this.getGraph().getView().getScale();
			super.paintGrid(g);
			Graphics2D g2 = (Graphics2D) g;
			g2.setColor(Color.gray);
			int x;
			int y = 0;
			int height = Math.max((int) this.getScaledPreferredSizeForGraph().getHeight(), this.getHeight());
			int width = Math.max((int) this.getScaledPreferredSizeForGraph().getWidth(), this.getWidth());
			while (y < height) {
				x = 0;
				while (x < width) {
					g2.fillRect(x-2, y-2, 4, 4);
					x += scale * 50;
				}
				y += scale * 50 ;
			}
		}

	}

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		mxGraph graph = new mxGraph() {

			@Override
			public boolean isCellMovable(Object cell) {
				if (((mxCell) cell).getValue() instanceof String) {
					String str = (String) ((mxCell) cell).getValue();
					if (str.contains("Port")) {
						return false;
					}
					return true;
				}
				return true;
			}

			@Override
			public boolean isCellEditable(Object cell) {
				return false;
			}
		};

		Object parent = graph.getDefaultParent();

		graph.getModel().beginUpdate();
		mxCell edge1;
		try {
			mxCell v1 = (mxCell) graph.insertVertex(parent, null, "Peter 1", 100, 100, 100, 200, "strokeWidth=0;strokeColor=#0f5ea2;fillColor=#ffdeff;fontColor=black;fontSize=18;verticalLabelPosition=top;verticalAlign=bottom");
			mxGeometry geoVertical = new mxGeometry(0, 100, 100, 20);
			mxCell leftPort = new mxCell("Port 1", geoVertical, "strokeWidth=0;shape=rectangle;strokeColor=#0f5ea2;fillColor=#d8eeff;fontSize=12;fontColor=black;portConstraint=eastwest;");
			leftPort.setVertex(true);
			leftPort.setConnectable(false);
			graph.addCell(leftPort, v1);
			mxCell vertex = new mxCell(null, new mxGeometry(0, 0, 100, 20), null);
			vertex.setVertex(true);
			graph.groupCells(vertex, 0, new Object[]{leftPort});

			mxCell v2 = (mxCell) graph.insertVertex(parent, null, "Peter 2", 400, 100, 100, 200, "strokeWidth=0;strokeColor=#0f5ea2;fillColor=#ffdeff;fontColor=black;fontBackgroundColor=red;fontSize=18;verticalLabelPosition=top;verticalAlign=bottom");
			mxCell v3 = (mxCell) graph.insertVertex(parent, null, "Peter 3", 700, 100, 100, 200, "strokeWidth=0;strokeColor=#0fafa2;fillColor=#ffdeff;fontColor=black;fontSize=18;verticalLabelPosition=top;verticalAlign=bottom");
			v1.setConnectable(false);
			v2.setConnectable(false);
			v3.setConnectable(false);

			edge1 = (mxCell) graph.insertEdge(parent, null, "WIRE 1", v1, v2, "strokeColor=red;fontSize=18;fontColor=blue;fontBackgroundColor=cyan;strokeWidth=3;dashed=true;");

//			edge1.getGeometry().setOffset(new mxPoint(0, 100));
			mxCell edge2 = (mxCell) graph.insertEdge(parent, null, "WIRE 2", v1, v3, "strokeColor=red;fontSize=18;strokeWidth=3;");
//			edge2.getGeometry().setOffset(new mxPoint(0, 100));

			mxCell edge3 = (mxCell) graph.insertEdge(parent, null, "WIRE 3", v2, v3, "strokeColor=red;fontSize=18;strokeWidth=3;");

//			edge.getGeometry().setTerminalPoint(new mxPoint(60, 180), true);
//			edge.getGeometry().setTerminalPoint(new mxPoint(230, 20), false);
//			edge.getGeometry().setPoints(new ArrayList<>());
//			edge.getGeometry().getPoints().add(new mxPoint(70, 50));
//			edge.getGeometry().getPoints().add(new mxPoint(120, 80));
			graph.setCellsDisconnectable(false);
			graph.setKeepEdgesInBackground(true);

			Map<String, Object> style = graph.getStylesheet().getDefaultEdgeStyle();
			style.put(mxConstants.STYLE_EDGE, mxEdgeStyle.ElbowConnectorPreventOverlap);
		} finally {
			graph.getModel().endUpdate();
		}

		MyGraphComponent graphComponent = new MyGraphComponent(graph);
		frame.getContentPane().add(BorderLayout.CENTER, graphComponent);
		frame.setSize(900, 600);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
