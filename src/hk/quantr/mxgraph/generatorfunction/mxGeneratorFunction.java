package hk.quantr.mxgraph.generatorfunction;

import hk.quantr.mxgraph.analysis.mxICostFunction;

/**
 * @author Mate
 * A parent class for all generator cost functions that are used for generating edge weights during graph generation
 */
public abstract class mxGeneratorFunction implements mxICostFunction
{
}
