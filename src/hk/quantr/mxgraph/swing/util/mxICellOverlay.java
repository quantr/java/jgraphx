package hk.quantr.mxgraph.swing.util;

import hk.quantr.mxgraph.util.mxRectangle;
import hk.quantr.mxgraph.view.mxCellState;

public interface mxICellOverlay
{

	/**
	 * 
	 */
	mxRectangle getBounds(mxCellState state);

}
